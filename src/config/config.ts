import {Settings} from 'luxon';

Settings.defaultZoneName = 'utc';

export default {
	mongo: {
		uri: process.env.MONGO_URI != null ? process.env.MONGO_URI : 'mongodb://localhost/stalker',
	},
	checkDuration: {
		minutes: 1,
	},
};
