import {DateTime} from 'luxon';

const log = (text: string) => {
	console.log(`${DateTime.utc().toFormat('d.L.yyyy H:mm:ss')}: ${text}`);
};

export {
	log,
};
