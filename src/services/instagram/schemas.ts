export const userInfoSchema = {
	type: 'object',
	required: [
		'id',
		'username',
		'profile_pic_url',
		'profile_pic_url_hd',
		'is_private',
		'followed_by_viewer',
		'follows_viewer',
		'requested_by_viewer',
		'edge_followed_by',
		'edge_follow',
		'edge_owner_to_timeline_media',
	],
	properties: {
		id: {
			type: 'string',
		},
		username: {
			type: 'string',
		},
		full_name: {
			type: 'string',
		},
		biography: {
			type: 'string',
		},
		profile_pic_url: {
			type: 'string',
		},
		profile_pic_url_hd: {
			type: 'string',
		},
		is_private: {
			type: 'boolean',
		},
		followed_by_viewer: {
			type: 'boolean',
		},
		follows_viewer: {
			type: 'boolean',
		},
		requested_by_viewer: {
			type: 'boolean',
		},
		edge_followed_by: {
			type: 'object',
			required: ['count'],
			properties: {
				count: {
					type: 'number',
				},
			},
		},
		edge_follow: {
			type: 'object',
			required: ['count'],
			properties: {
				count: {
					type: 'number',
				},
			},
		},
		edge_owner_to_timeline_media: {
			type: 'object',
			required: ['count'],
			properties: {
				count: {
					type: 'number',
				},
			},
		},
	},
};
