import * as superagent from 'superagent';
import {InstagramUserInfoResponse} from './types';
import * as ajv from '../../ajv';
import {userInfoSchema} from './schemas';
import * as instagramUserFacade from '../../model/instagramUser';

//TODO Errors, not found....
const getUserInfo = async (username: string): Promise<instagramUserFacade.IInstagramUser | null> => {
	try {
		const response = await superagent
			.get(`https://www.instagram.com/${username}`);

		const userData = JSON.parse(response.text.split("window._sharedData = ")[1].split(";</script>")[0]).entry_data.ProfilePage[0].graphql;

		if (userData != null && userData.user != null) {
			await ajv.validate(userInfoSchema, userData.user);

			return mapResponseInstagramUserToInstagramUser(userData.user);
		}

		return null;
	}
	catch (e) {
		return null;
	}
};

const mapResponseInstagramUserToInstagramUser = (instagramUserInfoResponse: InstagramUserInfoResponse): instagramUserFacade.IInstagramUser => {
	return {
		instagramUserId: instagramUserInfoResponse.id,
		username: instagramUserInfoResponse.username,
		fullName: instagramUserInfoResponse.full_name,
		biography: instagramUserInfoResponse.biography,
		profilePicUrl: instagramUserInfoResponse.profile_pic_url,
		profilePicUrlHd: instagramUserInfoResponse.profile_pic_url_hd,
		isPrivate: instagramUserInfoResponse.is_private,
		followersCount: instagramUserInfoResponse.edge_followed_by.count,
		followsCount: instagramUserInfoResponse.edge_follow.count,
		timelineMediaCount: instagramUserInfoResponse.edge_owner_to_timeline_media.count,
		followedByViewer: instagramUserInfoResponse.followed_by_viewer,
		followsViewer: instagramUserInfoResponse.follows_viewer,
		requestedByViewer: instagramUserInfoResponse.requested_by_viewer,
	};
};

export {
	getUserInfo,
};
