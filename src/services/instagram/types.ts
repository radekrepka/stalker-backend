export interface InstagramUserInfoResponse {
	readonly id: string;
	readonly username: string;
	readonly full_name: string;
	readonly biography: string;
	readonly profile_pic_url: string;
	readonly profile_pic_url_hd: string;
	readonly is_private: boolean;
	readonly followed_by_viewer: boolean;
	readonly follows_viewer: boolean;
	readonly requested_by_viewer: boolean;
	readonly edge_followed_by: {
		readonly count: number;
	};
	readonly edge_follow: {
		readonly count: number;
	};
	readonly edge_owner_to_timeline_media: {
		readonly count: number;
	};
}
