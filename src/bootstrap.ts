import {Duration} from 'luxon';
import mongoose from 'mongoose';
import config from './config';
import IntervalRunner from './IntervalRunner';
import * as logger from './utils/logger';
import * as instagramUserWatcher from './watchers/instagramUserWatcher';

logger.log('Starting stalker app...');

logger.log('Connecting to MongoDB...');
mongoose.connect(config.mongo.uri, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})
	.then(() => logger.log('Connected.'))
	.catch(() => logger.log('Failed to connect to MongoDB.'));

const intervalRunner = new IntervalRunner();

intervalRunner.addJob({
	callback: async (): Promise<void> => {
		try {
			logger.log('Checking...');
			await instagramUserWatcher.check();
		} catch (e) {
			logger.log(e.message);
		}
	},
	interval: Duration.fromObject(config.checkDuration),
});

intervalRunner.start();
