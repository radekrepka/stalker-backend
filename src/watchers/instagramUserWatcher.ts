import * as logger from '../utils/logger';
import * as watchedInstagramUserFacade from '../model/watchedInstagramUser';
import * as instagramUserFacade from '../model/instagramUser';
import * as instagramService from '../services/instagram/instagramService';

const check = async (): Promise<void> => {
	const watchedInstagramUsers = await watchedInstagramUserFacade.WatchedInstagramUser.find().exec();

	if (watchedInstagramUsers.length <= 0) {
		logger.log('No users for checking.');
		return;
	}

	await Promise.all(watchedInstagramUsers.map(async (watchedInstagramUser): Promise<void> => {
		const instagramUser = await instagramUserFacade.InstagramUser.findOne({
			instagramUserId: watchedInstagramUser.instagramUserId,
		}).exec();

		if (instagramUser == null) {
			throw Error(`Watched instagram user with instagramId ${watchedInstagramUser.instagramUserId} not found in InstagramUser schema.`);
		}

		const instagramUserInfo = await instagramService.getUserInfo(instagramUser.username);

		if (instagramUserInfo == null) {
			throw Error(`${instagramUser.username}: Getting instagram user info failed.`);
		}

		await checkFollowers(instagramUserInfo, instagramUser);
		await checkFollows(instagramUserInfo, instagramUser);
	}));
};

const checkFollowers = async (userInfo: instagramUserFacade.IInstagramUser, instagramUser: instagramUserFacade.IInstagramUser): Promise<void> => {
	if (userInfo.followersCount != instagramUser.followersCount) {
		logger.log(`${instagramUser.username}: Old followers count: ${instagramUser.followersCount}, new followers count: ${userInfo.followersCount}`);

		if (userInfo.followersCount > instagramUser.followersCount) {
			const newFollowersCount = userInfo.followersCount - instagramUser.followersCount;

			//TODO show followers names
			logger.log(`${instagramUser.username}: New followers: ${newFollowersCount}`);
		}

		if (userInfo.followersCount < instagramUser.followersCount) {
			const newFollowersCount = instagramUser.followersCount - userInfo.followersCount;

			//TODO show followers names
			logger.log(`${instagramUser.username}: Escaped followers count: ${newFollowersCount}`);
		}
	}
};

const checkFollows = async (userInfo: instagramUserFacade.IInstagramUser, instagramUser: instagramUserFacade.IInstagramUser): Promise<void> => {
	if (userInfo.followsCount != instagramUser.followsCount) {
		logger.log(`${instagramUser.username}: Old follows count: ${instagramUser.followsCount}, new follows count: ${userInfo.followsCount}`);

		if (userInfo.followsCount > instagramUser.followsCount) {
			const newFollowsCount = userInfo.followsCount - instagramUser.followsCount;

			//TODO show follows names
			logger.log(`${instagramUser.username}: New follows: ${newFollowsCount}`);
		}

		if (userInfo.followsCount < instagramUser.followsCount) {
			const newFollowsCount = instagramUser.followsCount - userInfo.followsCount;

			//TODO show follows names
			logger.log(`${instagramUser.username}: Stop follows: ${newFollowsCount}`);
		}
	}
};

export {
	check,
};
