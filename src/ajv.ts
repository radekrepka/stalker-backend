import Ajv from 'ajv';

const ajv = new Ajv();

const validate = async (schema: object | string | boolean, data: any): Promise<void> => { // eslint-disable-line @typescript-eslint/no-explicit-any
	const valid: boolean = await ajv.validate(schema, data);

	if (!valid) {
		throw new Error(ajv.errorsText());
	}
};

export {
	validate,
};
