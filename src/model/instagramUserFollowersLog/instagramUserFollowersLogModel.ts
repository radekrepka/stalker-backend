import {model, Schema} from 'mongoose';
import {InstagramUserFollowerAction, InstagramUserFollowersLogModel} from './types';
import {DateTime} from 'luxon';

const InstagramUserFollowersLogSchema: Schema = new Schema({
	instagramUserId: {
		type: String,
		required: true,
	},
	followersCount: {
		type: Number,
		required: true,
	},
	action: {
		type: String,
		enum: [
			InstagramUserFollowerAction.FOLLOW,
			InstagramUserFollowerAction.UNFOLLOW
		],
		required: true,
	},
	createdTime: {
		type: Date,
		default: DateTime.utc(),
	}
});

export default model<InstagramUserFollowersLogModel>('InstagramUserFollowersLog', InstagramUserFollowersLogSchema);
