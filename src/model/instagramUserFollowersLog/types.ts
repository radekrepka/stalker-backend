import {Document} from 'mongoose';
import {DateTime} from 'luxon';

export enum InstagramUserFollowerAction {
	FOLLOW = "FOLLOW",
	UNFOLLOW = "UNFOLLOW",
}

export interface InstagramUserFollowersLogModel extends Document {
	readonly instagramUserId: string;
	readonly followersCount: number;
	readonly action: InstagramUserFollowerAction;
	readonly createdTime: DateTime
}
