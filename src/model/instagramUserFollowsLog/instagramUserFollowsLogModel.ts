import {model, Schema} from 'mongoose';
import {InstagramUserFollowAction, InstagramUserFollowsLogModel} from './types';
import {DateTime} from 'luxon';

const InstagramUserFollowsLogSchema: Schema = new Schema({
	instagramUserId: {
		type: String,
		required: true,
	},
	followsCount: {
		type: Number,
		required: true,
	},
	action: {
		type: String,
		enum: [
			InstagramUserFollowAction.FOLLOW,
			InstagramUserFollowAction.UNFOLLOW
		],
		required: true,
	},
	createdTime: {
		type: Date,
		default: DateTime.utc(),
	}
});

export default model<InstagramUserFollowsLogModel>('InstagramUserFollowsLog', InstagramUserFollowsLogSchema);
