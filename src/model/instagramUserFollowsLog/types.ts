import {Document} from 'mongoose';
import {DateTime} from 'luxon';

export enum InstagramUserFollowAction {
	FOLLOW = "FOLLOW",
	UNFOLLOW = "UNFOLLOW",
}

export interface InstagramUserFollowsLogModel extends Document {
	readonly instagramUserId: string;
	readonly followsCount: number;
	readonly action: InstagramUserFollowAction;
	readonly createdTime: DateTime
}
