import {Document} from 'mongoose';
import {DateTime} from 'luxon';

export interface InstagramUserFollowModel extends Document {
	readonly instagramUserId: string;
	readonly instagramUserFollowId: string;
	readonly createdTime: DateTime
}
