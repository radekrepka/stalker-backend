import {model, Schema} from 'mongoose';
import {InstagramUserFollowModel} from './types';
import {DateTime} from 'luxon';

const InstagramUserFollowSchema: Schema = new Schema({
	instagramUserId: {
		type: String,
		required: true,
	},
	instagramUserFollowId: {
		type: String,
		required: true,
	},
	createdTime: {
		type: Date,
		default: DateTime.utc(),
	}
});

export default model<InstagramUserFollowModel>('InstagramUserFollow', InstagramUserFollowSchema);
