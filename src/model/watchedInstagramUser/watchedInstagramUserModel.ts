import {model, Schema} from 'mongoose';
import {WatchedInstagramUserModel} from './types';
import {DateTime} from 'luxon';

const WatchedInstagramUserSchema: Schema = new Schema({
	instagramUserId: {
		type: String,
		required: true,
	},
	createdTime: {
		type: Date,
		default: DateTime.utc(),
	}
});

export default model<WatchedInstagramUserModel>('WatchedInstagramUser', WatchedInstagramUserSchema);
