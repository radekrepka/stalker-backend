export {
	WatchedInstagramUserModel,
} from './types';

export {
	default as WatchedInstagramUser
} from './watchedInstagramUserModel';
