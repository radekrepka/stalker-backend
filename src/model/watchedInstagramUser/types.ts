import {Document} from 'mongoose';
import {DateTime} from 'luxon';

export interface WatchedInstagramUserModel extends Document {
	readonly instagramUserId: string;
	readonly createdTime: DateTime
}
