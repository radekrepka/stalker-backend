export {
	InstagramUser as IInstagramUser,
	InstagramUserModel,
} from './types';

export {
	default as InstagramUser
} from './instagramUserModel';
