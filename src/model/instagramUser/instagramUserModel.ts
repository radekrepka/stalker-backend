import {model, Schema} from 'mongoose';
import {InstagramUserModel} from './types';
import {DateTime} from 'luxon';

const InstagramUserSchema: Schema = new Schema({
	instagramUserId: {
		type: String,
		required: true,
	},
	username: {
		type: String,
		required: true,
	},
	fullName: {
		type: String,
		required: false,
		default: '',
	},
	biography: {
		type: String,
		required: false,
		default: '',
	},
	profilePicUrl: {
		type: String,
		required: true,
	},
	profilePicUrlHd: {
		type: String,
		required: true,
	},
	isPrivate: {
		type: Boolean,
		required: true,
	},
	followersCount: {
		type: Number,
		required: true,
	},
	followsCount: {
		type: Number,
		required: true,
	},
	timelineMediaCount: {
		type: Number,
		required: true,
	},
	followedByViewer: {
		type: Boolean,
		required: true,
	},
	followsViewer: {
		type: Boolean,
		required: true,
	},
	requestedByViewer: {
		type: Boolean,
		required: true,
	},
	createdTime: {
		type: Date,
		default: DateTime.utc(),
	}
});

export default model<InstagramUserModel>('InstagramUser', InstagramUserSchema);
