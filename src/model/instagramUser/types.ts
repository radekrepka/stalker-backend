import {Document} from 'mongoose';
import {DateTime} from 'luxon';

export interface InstagramUser {
	readonly instagramUserId: string;
	readonly username: string;
	readonly fullName: string | null;
	readonly biography: string | null;
	readonly profilePicUrl: string;
	readonly profilePicUrlHd: string;
	readonly isPrivate: boolean;
	readonly followersCount: number;
	readonly followsCount: number;
	readonly timelineMediaCount: number;
	readonly followedByViewer: boolean;
	readonly followsViewer: boolean;
	readonly requestedByViewer: boolean;
}

export interface InstagramUserModel extends Document, InstagramUser {
	readonly createdTime: DateTime
}
