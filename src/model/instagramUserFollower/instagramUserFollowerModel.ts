import {model, Schema} from 'mongoose';
import {InstagramUserFollowerModel} from './types';
import {DateTime} from 'luxon';

const InstagramUserFollowerSchema: Schema = new Schema({
	instagramUserId: {
		type: String,
		required: true,
	},
	instagramUserFollowerId: {
		type: String,
		required: true,
	},
	createdTime: {
		type: Date,
		default: DateTime.utc(),
	}
});

export default model<InstagramUserFollowerModel>('InstagramUserFollower', InstagramUserFollowerSchema);
