import {Document} from 'mongoose';
import {DateTime} from 'luxon';

export interface InstagramUserFollowerModel extends Document {
	readonly instagramUserId: string;
	readonly instagramUserFollowerId: string;
	readonly createdTime: DateTime
}
