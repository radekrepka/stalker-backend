import {Document} from 'mongoose';
import {DateTime} from 'luxon';

export interface InstagramUserPrivateAccountLogModel extends Document {
	readonly instagramUserId: string;
	readonly status: boolean;
	readonly createdTime: DateTime
}
