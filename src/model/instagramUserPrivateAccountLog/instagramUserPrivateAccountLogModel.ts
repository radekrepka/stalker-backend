import {model, Schema} from 'mongoose';
import {InstagramUserPrivateAccountLogModel} from './types';
import {DateTime} from 'luxon';

const InstagramUserPrivateAccountLogSchema: Schema = new Schema({
	instagramUserId: {
		type: String,
		required: true,
	},
	status: {
		type: Boolean,
		required: true,
	},
	createdTime: {
		type: Date,
		default: DateTime.utc(),
	}
});

export default model<InstagramUserPrivateAccountLogModel>('InstagramUserPrivateAccountLog', InstagramUserPrivateAccountLogSchema);
