import {Duration} from 'luxon';

interface JobCallback {
	(): Promise<void>;
}

interface Job {
	readonly callback: JobCallback;
	readonly interval: Duration;
}

interface JobStatus {
	readonly job: Job;
	runningInterval: NodeJS.Timeout | null;
}

export default class IntervalRunner {
	private readonly jobStatuses: JobStatus[];

	public constructor() {
		this.jobStatuses = [];
	}

	public addJob(job: Job): void {
		this.jobStatuses.push({
			job: job,
			runningInterval: null,
		});
	}

	public start(): void {
		for (const jobStatus of this.jobStatuses) {
			if (jobStatus.runningInterval == null) {
				jobStatus.runningInterval = this.runJob(jobStatus.job);
			}
		}
	}

	private runJob(job: Job): NodeJS.Timeout {
		return setInterval(async () => {
			try {
				await job.callback();
			} catch (error) {
				console.log(error);
			}
		}, job.interval.as('milliseconds'));
	}
}
